#!/usr/bin/python3

# I automatically import and build debian packages

# structure
#
# Supports
# Debian/Debian archive (this includes PPA)
#
# Config example
#
# mir:
#   suite:
#     - bionic
#   version: 1.1.0
#   archive: http://ppa.launchpad.net/mir-team/rc/ubuntu/

# NOTE: This has not been in any way optimimazed, so will be slow

import yaml
import requests
from io import BytesIO
import gzip
import tarfile
import re
import os
import configparser
import jenkins
import time
import sys

SOURCES = "{}/dists/{}/{}/source/Sources.gz"
MYFOLD = ".aidpkg/{}"
OUTPUT = ".aidpkg/sources/{}"
CHROOT_DIR = ".aidpkg/chroot/{}"


class Jenkins(object):
    def __init__(self, conf):
        self.server = jenkins.Jenkins(conf["url"], username=conf["username"],
                                      password=conf["password"])
        if not self.server.get_job_name("aidpkg"):
            self.server.create_job('aidpkg', jenkins.EMPTY_FOLDER_XML)
        self.jobs = []

    def has_job(self, package, suite):
        return self.server.get_job_name("aidpkg/{}-{}".format(package, suite))

    def update_if_needed(self, package, suite):
        tconf = self.server.get_job_config("aidpkg/template")
        conf = self.server.get_job_config(
            "aidpkg/{}-{}".format(package, suite))
        if tconf != conf:
            self.server.reconfig_job(
                "aidpkg/{}-{}".format(package, suite), tconf)

    def create_job(self, package, suite):
        conf = self.server.get_job_config("aidpkg/template")
        self.server.create_job("aidpkg/{}-{}".format(package, suite), conf)

    def create_job_if_none(self, package, suite):
        if self.has_job(package, suite) is None:
            self.create_job(package, suite)
        else:
            self.update_if_needed(package, suite)

    def build(self, package, suite, files, backports=False, branch=False):
        job = "aidpkg/{}-{}".format(package, suite)
        self.create_job_if_none(package, suite)
        para = {
            "FILES": files,
            "SUITE": suite
        }

        if backports:
            para["BACKPORTS"] = "true"

        if branch:
            para["BRANCH"] = branch

        self.server.build_job(job, parameters=para)
        next_build_number = self.server.get_job_info(job)['nextBuildNumber']
        self.jobs.append({
            "n": next_build_number,
            "package": package,
            "suite": suite,
            "package_suite": "{}+{}".format(package, suite)
        })

    def finished_jobs(self):
        finished = []
        list = []
        for job in self.jobs:
            jjob = "aidpkg/{}-{}".format(job["package"], job["suite"])
            jinfo = self.server.get_build_info(jjob, job["n"])
            if not jinfo["building"]:
                pjob = job
                if jinfo["result"] == "SUCCESS":
                    pjob["success"] = True
                else:
                    pjob["success"] = False
                finished.append(pjob)
                list.append("{}+{}".format(job["package"], job["suite"]))
        return {
            "pkg": finished,
            "list": list
        }

    def get_conf(self, job):
        return self.server.get_job_config(job)


class Ctrl(object):
    def __init__(self):
        self.file = MYFOLD.format('ctrl.ini')
        if not os.path.exists(MYFOLD.format("")):
            os.makedirs(MYFOLD.format(""))
        self.read()

    def get(self):
        return self.config["packages"]

    def update(self, package, version):
        self.config["packages"][package] = str(version)
        self.write()

    def isNew(self, package, version):
        if package not in self.get():
            return True
        if self.get()[package] != version:
            return True
        return False

    def write(self):
        with open(self.file, 'w') as f:
            self.config.write(f)

    def read(self):
        config = configparser.ConfigParser()
        if os.path.exists(self.file):
            config.read(self.file)
        if "packages" not in config:
            config["packages"] = {}
        self.config = config


def debug(*p):
    print("DEBUG: {}".format(' '.join(map(str, p))))


def error(*p):
    print("ERROR: {}".format(' '.join(map(str, p))))


def warning(*p):
    print("WARNING: {}".format(' '.join(map(str, p))))


def sorted_nicely(an_iterable):
    def convert(text): return int(text) if text.isdigit() else text

    def alphanum_key(key): return [convert(c)
                                   for c in re.split('([0-9]+)', key["version"])]
    return sorted(an_iterable, key=alphanum_key)


# Crude find latest, MAY NOT ALWAYS BE CORRECT!
def find_latest_version(packages):
    return sorted_nicely(packages)[-1]


def find_version(packages, version):
    for package in packages:
        if package["version"] == version or package["version"].split("-")[0] == version:
            return package
    return False


def format_package(package):
    splited = package.split("\n")
    formated = {}
    last = False
    for line in splited:
        if line.startswith(" ") and last:
            formated[last].append(line.split(" ")[-1])
        if not line.startswith(" ") and last:
            last = False
        for i in ["Package", "Version", "Files", "Format", "Directory"]:
            if line.startswith("{}: ".format(i)):
                formated[i.lower()] = line.split("{}: ".format(i))[1]
                continue
            if line.startswith("{}:".format(i)):
                last = i.lower()
                formated[i.lower()] = []
    return formated


def find_packages_from_sources(sources, package):
    packages = sources.split("\n\n")
    found = []
    for pkg in packages:
        if pkg.startswith("Package: {}".format(package)):
            found.append(format_package(pkg))
    return found


def get_sources(archive, suite, component):
    debug("Requesting archive", SOURCES.format(archive, suite, component))
    response = requests.get(SOURCES.format(archive, suite, component))
    buffer = BytesIO(response.content)
    return gzip.GzipFile(fileobj=buffer).read().decode('utf8')


def download_file(url, path):
    local_filename = "{}/{}".format(path, url.split('/')[-1])
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                f.write(chunk)
    return local_filename


def download_and_extract(file, path):
    if file.endswith("tar.xz"):
        type = "xz"
    elif file.endswith("tar.gz"):
        type = "gz"
    else:
        return False
    tarball = download_file(file, path)
    tar = tarfile.open(tarball, "r:{}".format(type))
    tar.extractall(path=path)
    tar.close()


def download_source(pkg, suite, archive):
    debug("Dowloading", pkg["package"], "for", suite)
    quilt = pkg["format"] == "3.0 (quilt)"
    path = OUTPUT.format("{}-{}-{}".format(
        pkg["package"], pkg["version"], suite))

    if not os.path.exists(path):
        os.makedirs(path)

    if quilt:
        debug(pkg["package"], "is a quilt package")
        for file in pkg["files"]:
            if ".debian." in file:
                download_and_extract("{}/{}/{}".format(
                    archive, pkg["directory"], file), path)
            if ".orig." in file:
                download_file("{}/{}/{}".format(
                    archive, pkg["directory"], file), path)
    else:
        debug(pkg["package"], "is a normal package")
        for file in pkg["files"]:
            download_and_extract("{}/{}/{}".format(
                archive, pkg["directory"], file), path)
    debug("Dowload of", pkg["package"], "done", "for", suite)


def need_backports(package):
    if "backports" in package:
        return package["backports"]
    return False


def need_branch(package):
    if "branch" in package:
        return package["branch"]
    return False


def request_build(pkg, suite, package, jserver):
    debug("Requesting build for", pkg["package"], "for", suite)
    quilt = pkg["format"] == "3.0 (quilt)"

    archive = package["from"]
    backports = need_backports(package)
    branch = need_branch(package)
    deb_tarball = False
    dsc_file = False
    orig_tarball = False
    files = []
    for file in pkg["files"]:
        files.append("{}/{}/{}".format(
            archive, pkg["directory"], file))
        if file.endswith(".dsc"):
            dsc_file = True
        elif ".debian." in file:
            quilt = True
            deb_tarball = True
        elif ".orig." in file:
            orig_tarball = True
        elif ".diff." in file:
            deb_tarball = True
        else:
            deb_tarball = True
    if quilt and not orig_tarball:
        error("Quilt package missing orig and debian tarballs!")
        return
    if not deb_tarball:
        error("Package missing source tarball!")
        return
    if not dsc_file:
        error("Missing dsc file!")
        return

    if quilt:
        debug(pkg["package"], "is a quilt package")
    else:
        debug(pkg["package"], "is a normal package")
    jserver.build(pkg["package"], suite,
                  " ".join(files), backports=backports, branch=branch)
    debug("Request to build", pkg["package"], "done", "for", suite)

def get_archive_from_conf(conf, archive):
    if "archives" not in conf:
        if archive.startswith("http"):
            return archive
        error("Archive", archive, "NOT FOUND")
        return False

    for a in conf["archives"]:
        if a == archive:
            return conf["archives"][a]["main"]
    if archive.startswith("http"):
        return archive

    error("Archive", archive, "NOT FOUND")
    return False


waiting_pkg = []
versions = {}

CONF_FILE = "aidpkg.yaml"
if len(sys.argv) > 1:
    CONF_FILE = sys.argv[1]

if not os.path.exists(CONF_FILE):
    error("Cannot find", CONF_FILE)
    exit()

with open(CONF_FILE, 'r') as stream:
    try:
        conf = yaml.load(stream)
    except yaml.YAMLError as exc:
        print(exc)

ctrl = Ctrl()
jconf = conf["jenkins"]
jserver = Jenkins(jconf)
for _package in conf["packages"]:
    package = conf["packages"][_package]
    for raw_suite in package["suites"]:
        # from:to , example sid:xenial
        suite_from, suite = raw_suite, raw_suite
        if ":" in suite:
            suite_from, suite = raw_suite.split(":")
        pkg_suite = "{}+{}".format(_package, suite)
        debug("Processing package", pkg_suite)
        if "component" not in package:
            package["component"] = "main"
        debug("From suite", suite_from, "To suite", suite, "in", package["component"])
        package["from"] = get_archive_from_conf(conf, package["from"])
        raw_source = get_sources(
            package["from"], suite_from, package["component"])
        pkgs = find_packages_from_sources(raw_source, _package)

        if len(pkgs) <= 0:
            error("Could not find", pkg_suite, "in", package["from"])
            exit()

        if "version" in package:
            pkg = find_version(pkgs, package["version"])
            if not pkg:
                warning("Could not find version",
                        package["version"], "of", pkg_suite, "IGNORING")
                print("")
                continue
            debug("Using version:", pkg["version"])
        else:
            pkg = find_latest_version(pkgs)
            if not pkg:
                warning("No version found for", pkg_suite, "IGNORING")
                print("")
                continue
            debug("Latest version is:", pkg["version"])

        if not ctrl.isNew(pkg_suite, pkg["version"]):
            debug("Alredy corect/latest version of", pkg_suite)
            print("")
            continue

        versions[pkg_suite] = pkg["version"]

        if "after" in package:
            # We need to wait for another package(s)
            suite_after = []
            for after in package["after"]:
                suite_after.append("{}+{}".format(after, suite))
            for after in suite_after:
                waiting_pkg.append({
                    "pkg": pkg,
                    "package": package,
                    "suite": suite,
                    "for": suite_after
                })
            debug(pkg_suite, "is wating for", " ".join(suite_after))
            print("")
            continue

        request_build(pkg, suite, package, jserver)

        # If we want to build localy sometime
        # download_source(pkg, suite, package["from"])

        print("")

if len(waiting_pkg) is not 0:
    while True:
        debug("Sleep for 30s until next check")
        time.sleep(30)
        debug("Checking for finished jobs")
        finished = jserver.finished_jobs()
        for job in finished["list"]:
            ctrl.update(job,
                        versions[job])
        for wait in waiting_pkg:
            FINISHED = True
            for w in wait["for"]:
                if w not in finished["list"]:
                    FINISHED = False
            if FINISHED:
                ALL_SUCESS = True
                for job in finished["pkg"]:
                    if job["package_suite"] in wait["for"]:
                        if not job["success"]:
                            ALL_SUCESS = False
                if ALL_SUCESS:
                    request_build(wait["pkg"], wait["suite"],
                                  wait["package"], jserver)
                else:
                    error("FAILED TO BUILD")

                waiting_pkg.remove(wait)
                if len(waiting_pkg) == 0:
                    debug("Finished")
                    exit()
        pass

for job in jserver.jobs:
    ctrl.update(job["package_suite"], versions[job["package_suite"]])
